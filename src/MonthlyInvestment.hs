{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE TypeFamilies      #-}


module MonthlyInvestment (calculate_investment_needed) where

--- Basic Calculation

-- Calculate formula type 1
calculate_monthly_interest annual_interest_rate = (annual_interest_rate/(100 * 12))
-- Calculate formula type 2
calculate_interest_rate monthly_interest total_month = ((1 + monthly_interest) ** (total_month))
-- Calculate formula type 3
calculate_interest_rate_y interest_rate = interest_rate - 1
-- Calculate formula type 4
calculate_interest_from_initial initial_deposit interest_rate = initial_deposit * interest_rate
-- Successor
suc x = x + 1

-- Monthly Investment Calculation
calculate_investment_needed :: Float -> Float -> Float -> Float -> Int
calculate_investment_needed invest_target initial_deposit annual_interest_rate total_month = round (((((invest_target) - (calculate_interest_from_initial initial_deposit (calculate_interest_rate (calculate_monthly_interest annual_interest_rate) total_month))) * (calculate_monthly_interest annual_interest_rate)) / (calculate_interest_rate_y (calculate_interest_rate (calculate_monthly_interest annual_interest_rate) total_month))) / (1 + calculate_monthly_interest(annual_interest_rate)))