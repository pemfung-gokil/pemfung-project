{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Main where

import Web.Scotty
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics
import System.Environment

import FutureInvestment (calc_future_investment)
import MonthlyInvestment (calculate_investment_needed)
import TimePeriod (calc_total_month_needed_helper)


data CalcParam = CalcParam { initialDeposit :: Float, monthlyDeposit :: Float, monthDuration :: Float, annualInterestRate :: Float } deriving (Show, Generic)
instance ToJSON CalcParam
instance FromJSON CalcParam

data ResultJson = ResultJson { val :: Float } deriving (Show, Generic)
instance ToJSON ResultJson
instance FromJSON ResultJson

data CalcParamMI = CalcParamMI { investTargetMI :: Float, initialDepositMI :: Float, annualInterestRateMI :: Float, totalMonthMI :: Float } deriving (Show, Generic)
instance ToJSON CalcParamMI
instance FromJSON CalcParamMI

data ResultJsonMI = ResultJsonMI { monthlyInvest :: Float } deriving (Show, Generic)
instance ToJSON ResultJsonMI
instance FromJSON ResultJsonMI

data CalcParamTP = CalcParamTP { investTargetTP :: Float, annualInterestRateTP ::Float, initialDepositTP :: Float, monthlyInvestmentTP :: Float } deriving (Show, Generic)
instance ToJSON CalcParamTP
instance FromJSON CalcParamTP

data ResultJsonTP = ResultJsonTP { timePeriod :: Float } deriving (Show, Generic)
instance ToJSON ResultJsonTP
instance FromJSON ResultJsonTP

data TestJson = TestJson { output :: String } deriving (Show, Generic)
instance ToJSON TestJson
instance FromJSON TestJson

main :: IO ()
main = do
  putStrLn "hello world"

  port <- read <$> getEnv "PORT"

  scotty port $ do
    post "/future-investment" $ do
      jsonRaw <- jsonData :: ActionM CalcParam

      let initDeposit = initialDeposit jsonRaw
          monDuration = monthDuration jsonRaw
          monthDeposit = monthlyDeposit jsonRaw
          annInterestRate = annualInterestRate jsonRaw

      let res = calc_future_investment annInterestRate monDuration initDeposit monthDeposit

      let result = ResultJson {val = res}
      json result

    post "/monthly-investment" $ do
      jsonRaw <- jsonData :: ActionM CalcParamMI

      let invTargetMI = investTargetMI jsonRaw
          initDepoMI = initialDepositMI jsonRaw
          annualIntRateMI = annualInterestRateMI jsonRaw
          ttlMonthMI = totalMonthMI jsonRaw

      let res = calculate_investment_needed invTargetMI initDepoMI annualIntRateMI ttlMonthMI

      let result = ResultJsonMI {monthlyInvest = res}
      json result

    post "/time-period" $ do
      jsonRaw <- jsonData :: ActionM CalcParamTP

      let invTargetTP = investTargetTP jsonRaw
          annInterestRateTP = annualInterestRateTP jsonRaw
          -- stTP = startTP jsonRaw
          initDepositTP = initialDepositTP jsonRaw
          monthInvestmentTP = monthlyInvestmentTP jsonRaw

      let res = calc_total_month_needed_helper invTargetTP annInterestRateTP 1.0 initDepositTP monthInvestmentTP

      let result = ResultJsonTP {timePeriod = res}
      json result

    get "/test" $ do
      let result = TestJson { output = "test" }
      json result
