{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE TypeFamilies      #-}

module FutureInvestment (calc_future_investment) where

-- Bunga per bulan
calculate_monthly_interest annual_interest_rate = (annual_interest_rate/(100 * 12))
-- Bunga sesuai input jml bulan
calculate_interest_rate monthly_interest total_month = ((1 + monthly_interest) ** (total_month))
-- Interest Rate Y
calculate_interest_rate_y interest_rate = interest_rate - 1
-- Bunga didapat dr First_Investment (Pendanaan Awal)
calculate_interest_from_initial initial_deposit interest_rate = initial_deposit * interest_rate
-- Successor
suc x = x + 1

-- Kalkulasi untuk Invest Projection Based on Money Invest Plan
-- Step kalkulasi 1
calculate_first monthly_investment interest_rate_y monthly_interest = monthly_investment * interest_rate_y / monthly_interest
-- Step kalkulasi 2
-- calculate_second interest_from_initial first monthly_interest = round $ (interest_from_initial + (first * (1 + monthly_interest)))
calculate_second :: Float -> Float -> Float -> Float
calculate_second interest_from_initial first monthly_interest = interest_from_initial + (first * (1 + monthly_interest))

-- Kalkulasi untuk Month needed to get Investment Target
-- calc_future_investment annual_interest_rate total_month initial_deposit monthly_investment =
--     calculate_second (calculate_interest_from_initial initial_deposit (calculate_interest_rate (calculate_monthly_interest annual_interest_rate) total_month)) (calculate_first monthly_investment (calculate_interest_rate_y (calculate_interest_rate (calculate_monthly_interest annual_interest_rate) total_month)) (calculate_monthly_interest annual_interest_rate)) (calculate_monthly_interest annual_interest_rate)

calc_future_investment :: Float -> Float -> Float -> Float -> Float
calc_future_investment annual_interest_rate total_month initial_deposit monthly_investment =
    calculate_second (calculate_interest_from_initial initial_deposit (calculate_interest_rate (calculate_monthly_interest annual_interest_rate) total_month)) (calculate_first monthly_investment (calculate_interest_rate_y (calculate_interest_rate (calculate_monthly_interest annual_interest_rate) total_month)) (calculate_monthly_interest annual_interest_rate)) (calculate_monthly_interest annual_interest_rate)
