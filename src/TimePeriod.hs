{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE TypeFamilies      #-}

module TimePeriod (calc_total_month_needed_helper) where

    -- Monthly Interest
    calculate_monthly_interest annual_interest_rate = (annual_interest_rate/(100 * 12))
    -- Interest corresponds to the input of the number of months
    calculate_interest_rate :: Float -> Float -> Float
    calculate_interest_rate monthly_interest total_month = ((1 + monthly_interest) ** (total_month))
    -- Interest Rate Y
    calculate_interest_rate_y :: Float -> Float
    calculate_interest_rate_y interest_rate = interest_rate -1
    -- Interest from First_Investment (Pendanaan Awal)
    calculate_interest_from_initial initial_deposit interest_rate = initial_deposit * interest_rate
    -- Successor
    suc x = x + 1

    -- First Step
    calculate_first monthly_investment interest_rate_y monthly_interest = monthly_investment * interest_rate_y / monthly_interest
    -- Second Step
    calculate_second :: Float -> Float -> Float -> Float
    calculate_second interest_from_initial first monthly_interest = (interest_from_initial + (first * (1 + monthly_interest)))

    -- Month needed to get Investment Target Calculator
    calc_future_investment :: Float -> Float -> Float -> Float -> Float
    calc_future_investment annual_interest_rate total_month initial_deposit monthly_investment =
        calculate_second (calculate_interest_from_initial initial_deposit (calculate_interest_rate (calculate_monthly_interest annual_interest_rate) total_month)) (calculate_first monthly_investment (calculate_interest_rate_y (calculate_interest_rate (calculate_monthly_interest annual_interest_rate) total_month)) (calculate_monthly_interest annual_interest_rate)) (calculate_monthly_interest annual_interest_rate)

    calc_total_month_needed invest_target annual_interest_rate initial_deposit monthly_investment =
        calc_total_month_needed_helper invest_target annual_interest_rate 1 initial_deposit monthly_investment

    -- Main recursive function
    calc_total_month_needed_helper :: Float -> Float -> Float -> Float -> Float -> Float
    calc_total_month_needed_helper invest_target annual_interest_rate start initial_deposit monthly_investment = 
        if invest_target <= calc_future_investment annual_interest_rate start initial_deposit monthly_investment 
            then start 
            else calc_total_month_needed_helper invest_target annual_interest_rate (suc start) initial_deposit monthly_investment