# pemfung-project

This is Backend Repository

## Link Presentasi

http://bit.ly/ppt-pemfung-kelompok-13

## Step by step

Kalkulator finansial berfungsi untuk menghitung rencana pendaan untuk mendapatkan kondisi keuangan yang lebih terencana di masa depan.

3 Produk :

Proyeksi hasil pendanaan
Hitung target dana yang kamu dapatkan dengan nominal pemberian pinjaman bulanan dan jangka waktu yang telah kamu tentukan.

Step 1 : Masukkan nominal pendanaan awal yang akan kamu berikan
Step 2 : Masukkan rencana pendanaan bulanan yang akan kamu berikan
Step 3 : Masukkan persenan imbal hasil per tahun yang kamu inginkan
Step 4 : Tentukan jangka waktu yang kamu inginkan
Step 5 : Tekan tombol hitung
Step 6 : Akan keluar nominal proyeksi hasil pendanaan yang kamu dapatkan.

Jangka waktu
Hitung jangka waktu yang diperlukan jika kamu rutin memberikan pinjaman bulanan dengan nominal yang telah ditentukan untuk mendapatkan target danamu.

Step 1 : Masukkan target dana yang kamu inginkan
Step 2 : Masukkan nominal pendanaan awal yang akan kamu berikan
Step 3 : Masukkan rencana pendanaan bulanan yang akan kamu berikan
Step 4 : Masukkan persenan imbal hasil per tahun yang kamu inginkan
Step 5 : Tekan tombol hitung
Step 6 : Akan keluar jangka waktu yang diperlukan untuk mencapai target dana yang kamu inginkan

Nominal pendanaan bulanan
Hitung nominal besaran pemberian pinjaman per bulan yang harus kamu berikan untuk mencapai target dana dalam jangka waktu yang kamu tentukan.

Step 1 : Masukkan target dana yang kamu inginkan
Step 2 : Masukkan nominal pendanaan awal yang akan kamu berikan
Step 3 : Masukkan persenan imbal hasil per tahun yang kamu inginkan
Step 4 : Tentukan jangka waktu yang kamu inginkan
Step 5 : Tekan tombol hitung
Step 6 : Akan keluar nominal pendanaan bulanan yang harus kamu berikan unutuk mencapai target dana dengan jangka waktu yang kamu inginkan.